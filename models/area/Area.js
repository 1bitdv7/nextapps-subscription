import mongoose from 'mongoose';

const AreaSchema = new mongoose.Schema({
  name: { type: String },
  place: { type: String }
});

export default mongoose.models.Area || mongoose.model('Area', AreaSchema);
