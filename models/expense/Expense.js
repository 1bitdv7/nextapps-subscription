import mongoose from 'mongoose';

const ExpenseSchema = new mongoose.Schema({
  lower: { type: String },
  proper: { type: String },
  picture: { type: String }
});

export default mongoose.models.Expense || mongoose.model('Expense', ExpenseSchema);
