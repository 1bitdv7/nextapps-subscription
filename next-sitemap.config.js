// const siteUrl = "https://www.qarrington.com"
const siteUrl = process.env.NEXT_PUBLIC_APP_URL;

module.exports = {
  siteUrl,
  generateRobotsTxt: true,
  exclude: [
    '/expenses/sitemap',
    '/expenses/sitemap/*',
    '/flight/sitemap',
    '/draft/sitemap',
    '/dashboard/draft/sitemap'
  ],
  robotsTxtOptions: {
    additionalSitemaps: [
      `${siteUrl}expenses/sitemap`,
      `${siteUrl}flight/sitemap`,
      `${siteUrl}draft/sitemap`
    ]
  },
  sitemapSize: 7000
};
