import React, { useState, useEffect } from 'react';
import {
  Avatar,
  Box,
  Container,
  Card,
  Grid,
  Tab,
  Tooltip,
  Typography,
} from '@mui/material';
import {
  TabContext,
  TabList,
} from '@mui/lab';
import Link from 'next/link';
import useSWR from 'swr';
import Marquee from "react-fast-marquee";

const Component = () => {

  const fetcher = (...args) => fetch(...args).then(res => res.json());
  const { data: subscriptions } = useSWR(`${process.env.NEXT_PUBLIC_APP_URL}/api/subscriptions`, fetcher)

  return (

    <Container>
      <Box sx={{ marginTop: "108px", marginBottom: 2 }}>
        <Box display="flex" justifyContent="center">
          <Marquee gradient={false} pauseOnHover={true}>
            <Grid item xs={12} mb={2}>
              <Grid container spacing={0}>

                {subscriptions && subscriptions.slice(0, 6).map(({ _id, subscriptionSlug, subscriptionListing, subscriptionPrice, subscriptionTrack }) => (
                  <Grid key={_id} mt={0} mb={-2} mx={0.5}>

                    <Link href={`/subscription/${subscriptionSlug}`}>
                      <Tooltip title={subscriptionListing.listingName} placement="top">
                        <Card style={{ padding: '16px', cursor: 'pointer' }}>
                          <Box
                            style={{
                              display: 'flex',
                              justifyContent: 'center'
                            }}
                          >
                            <Avatar
                              style={{ width: 24, height: 24 }}
                              alt={subscriptionListing.listingName}
                              src={subscriptionListing.listingLogo}
                            />
                          </Box>
                          <Box style={{ textAlign: 'center' }}>
                            <Box mt={0.5}>
                              <Typography component="span" variant="body2" color={subscriptionTrack.trackVariation} fontWeight={500}>
                                {subscriptionTrack.trackPercentInterval}
                              </Typography>
                              <Typography component="span" variant="body2" fontWeight={400} color="secondary">
                                %
                              </Typography>
                            </Box>
                          </Box>
                        </Card>
                      </Tooltip>
                    </Link>

                  </Grid>
                ))}

              </Grid>
            </Grid>

          </Marquee>
        </Box>
      </Box>
    </Container>

  )

}

export default Component;