import Router from 'next/router';
import { createContext, useEffect, useState } from 'react';
import { setCookie, parseCookies } from 'nookies';
import Api from '../lib/fetchApi';
export const AuthContext = createContext();

export function AuthProvaider({ children }) {
  const [user, setUser] = useState(null);

  const isAutenticated = !!user;

  function cookies(token,isAdmin) {
    setCookie(this,'token.Qarrington.bearer',isAdmin ? `${token} iStruedb`: token, {
      maxAge: 60 * 60 * 24 * 7,
      sameSite: true,
      path: '/'
    });
  }
  const { 'token.Qarrington.bearer': token, isAdmin } = parseCookies();
  useEffect(async () => {
    if(!token) {
      setUser(false)
      return
    }
    const userInformation = await Api('api/accounts/recoverUserInfo','POST',{token:token})
    setUser(userInformation.result);
    console.log(userInformation.result)
  }, []);

  async function signIn(data) {

    const { accountKey, token, isRegister, user } = data;

    if (!isRegister && isRegister == undefined) {

      if(user.accountIsAdmin && user.accountIsAdmin == true) {
        cookies(token,true);
        } else{ cookies(token,false) ; }
          // user data
          setUser(user);
          return true
    } else {
      // token and user info
      if(user.accountIsAdmin && user.accountIsAdmin == true){
        cookies(token,true);
      }else{ cookies(token,false); }
      
      setUser(accountKey);
      Router.push('/dashboard');
      
    }
  }

  async function logout (){
    console.log('logout');
    //setUser(false);
  }

  return (
    <AuthContext.Provider value={{ isAutenticated, signIn, user,logout }}>
      {children}
    </AuthContext.Provider>
  );
}
