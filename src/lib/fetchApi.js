import { parseCookies } from 'nookies';

export default async function Api(url, method, data = null, ctx = null) {
  const { ['token.Qarrington.bearer']: token } = parseCookies(ctx);
  let responseData = ''
  if (token && method === 'POST' || method === 'PUT' || method === 'DELETE') {
    responseData = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}${url}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      },
      method: method,
      body: JSON.stringify(data)
    });
    return { result: await responseData.json(), code: responseData.status}
  }else if( token && method === 'GET'){
    responseData = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}${url}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token
        },
        method: method
      });
      return { result: await responseData.json(), code: responseData.status}
  } else if(!token &&  method === 'GET'){
    responseData = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}${url}`, {
        headers: {
          'Content-Type': 'application/json'
        },
        method: method
      });
      return { result: await responseData.json(), code: responseData.status}
  }else{
    responseData = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}${url}`, {
        headers: {
          'Content-Type': 'application/json'
        },
        method: method,
        body: JSON.stringify(data)
      });
      return { result: await responseData.json(), code: responseData.status}
  }
}
