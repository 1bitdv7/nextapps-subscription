import Jwt from 'jsonwebtoken';

export  async function createTokenAccess(id) {
  return Jwt.sign({}, `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`, {
    algorithm: 'HS256',
    expiresIn: '7 days',
    subject: `${id}`
  });
}

export async function checkAccessToken(token){
    return Jwt.verify(token, `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`)
}
