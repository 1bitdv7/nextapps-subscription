import React, { useContext, useEffect, useState } from 'react';
import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';
import Carousel from 'react-material-ui-carousel';
import InfoRoundedIcon from '@mui/icons-material/InfoRounded';
import {
  Avatar,
  Badge,
  Box,
  Breadcrumbs,
  Button,
  Container,
  Grid,
  Hidden,
  Stack,
  styled,
  TextField,
  Tooltip,
  Typography
} from '@mui/material';
import useSWR from 'swr';
import { AuthContext } from '../../../context/Auth';
import Api from '../../../lib/fetchApi';
import Router from 'next/router';
import { parseCookies } from 'nookies';

const Page = () => {
  const fetcher = (...args) => fetch(...args).then((res) => res.json());
  const [data, setData] = useState({ hits: [] });
  useEffect(async () => {
    data = await Api('api/accounts', 'GET');
    setData(data);
  }, []);

  const { signIn } = useContext(AuthContext);
  const handlerSubmit = async (event) => {
    event.preventDefault();
    const { accountKey,login } = event.target;

    accountKey.setAttribute('disabled', 'disabled');
    login.setAttribute('disabled', 'disabled');
    const signInUser = await fetch(
      `${process.env.NEXT_PUBLIC_APP_URL}api/accounts/login`,
      {
        method: 'POST',
        headers: { Accept: 'application/json' },
        body: JSON.stringify({ accountKey: accountKey.value })
      }
    );

    if(signInUser.status == 401){
      accountKey.style.border = '1px solid red';
      accountKey.style.borderRadius = '10px';
      accountKey.removeAttribute('disabled', 'disabled');
      login.removeAttribute('disabled', 'disabled');
      return
    }
    const response = await signInUser.json();
    const { token, ...user } = response;

    const result = await signIn({
      token: token,
      accountKey: accountKey.value,
      user: user
    });

    if (result) {
      Router.push('/dashboard');
    }
  };
  return (
    <>
      <Head>
        <title>Access Account • Qarrington</title>
        <meta
          name="description"
          content="Qarrington's where users from 150+ countries spread expenses via subscriptions. No rates like loans. No rejections like insurance. See how much we'd cover."
        />
      </Head>

      <MainContent style={Body}>
        <Grid
          container
          sx={{ height: '100%' }}
          alignItems="stretch"
          spacing={0}
        >
          {/* left container starts */}

          <Grid
            xs={12}
            md={6}
            alignItems="center"
            display="flex"
            justifyContent="center"
            item
          >
            <Container maxWidth="sm">
              <Box style={{ textAlign: 'center' }}>
                <Box sx={{ cursor: 'pointer' }}>
                  <Link href="/">
                    <Image
                      alt="Qarrington Logo"
                      height={32}
                      width={32}
                      src="/assets/media/logos/aa/primary.png"
                    />
                  </Link>
                </Box>

                <Typography
                  fontSize="42px"
                  fontWeight="700"
                  lineHeight="50px"
                  component="div"
                  sx={{ my: 1 }}
                >
                  Access your account without personal data.
                </Typography>

                <Typography
                  variant="h6"
                  component="div"
                  color="secondary"
                  padding="0px 20px 0px 20px"
                  gutterBottom
                >
                  If in any circumstances you can't remember your accessToken,
                  just navigate to{' '}
                  <Link href="/account/retake">
                    <Typography
                      component="span"
                      color="primary"
                      variant="h6"
                      sx={{
                        cursor: 'pointer',
                        '&:hover': {
                          color: '#000'
                        }
                      }}
                    >
                      accountRetake
                    </Typography>
                  </Link>{' '}
                  and enter your secretToken. You'd need a new account if you
                  can't remember your secretToken.
                </Typography>
              </Box>

              <form noValidate autoComplete="on" onSubmit={handlerSubmit}>
                <Box
                  style={{ textAlign: 'center', padding: '14px 60px 0px 60px' }}
                >
                  <Stack spacing={1.2} sx={{ width: '100%' }}>
                    <TextField
                      sx={{ input: { textAlign: 'center' } }}
                      required
                      name="accountKey"
                      placeholder="Enter your accessToken"
                    />

                    <Button
                      size="large"
                      sx={{
                        color: 'white',
                        py: 1.6,
                        textTransform: 'uppercase',
                        fontSize: '13px'
                      }}
                      variant="contained"
                      fullWidth={true}
                      name="login"
                      type="submit"
                    >
                      Login
                    </Button>
                  </Stack>
                </Box>

                <Breadcrumbs
                  separator="/"
                  aria-label="breadcrumb"
                  sx={{
                    '& ol': {
                      justifyContent: 'center',
                      margin: 'auto',
                      mt: '20px'
                    }
                  }}
                >
                  <Link href="/account/create">
                    <Typography
                      variant="body2"
                      color="secondary"
                      sx={Breadcrumb}
                    >
                      create account
                    </Typography>
                  </Link>

                  <Link href="/account/retake">
                    <Typography
                      variant="body2"
                      color="secondary"
                      sx={Breadcrumb}
                    >
                      retake account
                    </Typography>
                  </Link>
                </Breadcrumbs>
              </form>
            </Container>
          </Grid>

          {/* left container ends */}

          {/* right container starts */}

          <Hidden mdDown>
            <GridWrapper
              xs={12}
              md={6}
              alignItems="center"
              display="flex"
              justifyContent="center"
              item
            >
              <Container maxWidth="sm">
                <Box textAlign="center">
                  <Carousel>
                    {data &&
                      Array.isArray(data) &&
                      data
                        .slice(0, 5)
                        .map(({ _id, accountProfile, accountReview }) => (
                          <Box key={_id}>
                            <Box
                              style={{
                                display: 'flex',
                                justifyContent: 'center'
                              }}
                            >
                              <StyledBadge
                                overlap="circular"
                                anchorOrigin={{
                                  vertical: 'bottom',
                                  horizontal: 'right'
                                }}
                                variant={accountProfile.profileIsActive}
                              >
                                <Avatar
                                  style={{ width: 80, height: 80 }}
                                  alt={accountProfile.profileName}
                                  src={accountProfile.profileAvatar}
                                />
                              </StyledBadge>
                            </Box>
                            <Box marginTop="16px">
                              <Typography
                                variant="h5"
                                component="div"
                                fontWeight="600"
                                gutterBottom
                              >
                                {accountProfile.profileName}
                              </Typography>
                              <Typography
                                variant="body"
                                component="div"
                                gutterBottom
                              >
                                {accountProfile.profileTitle}
                              </Typography>
                              <Typography
                                variant="h4"
                                component="div"
                                fontWeight="600"
                              >
                                {accountReview.reviewContent}
                              </Typography>
                            </Box>
                          </Box>
                        ))}
                  </Carousel>
                </Box>
              </Container>
            </GridWrapper>
          </Hidden>

          {/* right container ends */}
        </Grid>
      </MainContent>
    </>
  );
};

export default Page;

const MainContent = styled(Box)(
  () => `
    height: 100%;
    display: flex;
    flex: 1;
    overflow: auto;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`
);

const GridWrapper = styled(Grid)(
  ({ theme }) => `
    background: ${theme.colors.gradients.green2};
`
);

const Body = {
  backgroundColor: '#ffffff'
};

const StyledBadge = styled(Badge)(({ theme }) => ({
  '& .MuiBadge-badge': {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: 'ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""'
    }
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0
    }
  }
}));

const Breadcrumb = {
  cursor: 'pointer',
  fontWeight: '500',
  '&:hover': {
    color: '#000'
  }
};

export async function getServerSideProps(ctx) {
  const { ['token.Qarrington.bearer']: token } = parseCookies(ctx);
  if (token) {
    return {
      redirect: {
        destination: '/',
        permanent: false
      }
    };
  }
  return {
    props: {}
  };
}
