import React, { useContext, useState } from 'react';
import { AuthContext } from '../../../context/Auth';
import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';
import Carousel from 'react-material-ui-carousel';
import InfoRoundedIcon from '@mui/icons-material/InfoRounded';
import {
  Avatar,
  Badge,
  Box,
  Button,
  Container,
  Grid,
  Hidden,
  Stack,
  styled,
  TextField,
  Tooltip,
  Typography
} from '@mui/material';
import useSWR from 'swr';
import { useForm } from 'react-hook-form';
import { KeyboardReturn } from '@mui/icons-material';
import { parseCookies } from 'nookies';

const Page = () => {
  const fetcher = (...args) => fetch(...args).then((res) => res.json());
  const { data } = useSWR(
    `${process.env.NEXT_PUBLIC_APP_URL}api/accounts`,
    fetcher
  );

  const getSteps = () => {
    return ['Account Key'];
  };

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <Stack spacing={1.2} sx={{ width: '100%' }}>
            <Tooltip
              title="Your Account Key allows you to use Qarrington without exposing your email or number."
              placement="top"
            >
              <TextField
                {...register('accountkey')}
                sx={{ input: { textAlign: 'center' } }}
                required
                name="accountkey"
                placeholder="Account Key"
              />
            </Tooltip>
          </Stack>
        );

      default:
        return 'unknown step';
    }
  };

  const [activeStep, setActiveStep] = useState(0);
  const [skippedSteps, setSkippedSteps] = useState([]);
  const steps = getSteps();

  const { register } = useForm();
  const { signIn } = useContext(AuthContext);

  const handleSubmits = async (event) => {
    event.preventDefault();
    const { accountkey, register } = event.target;

    accountkey.setAttribute('disabled', 'disabled');
    register.setAttribute('disabled', 'disabled');

    register.setAttribute('value', ' Registering... ');
    const registerUser = await fetch(
      `${process.env.NEXT_PUBLIC_APP_URL}api/accounts/create`,
      {
        headers: { Accept: 'application/json' },
        method: 'POST',
        body: JSON.stringify({ accountKey: accountkey.value })
      }
    );
    if (registerUser.status == 401 || registerUser.status == 500) {
      accountkey.style.border = '1px solid red';
      accountkey.style.borderRadius = '10px';
      accountkey.removeAttribute('disabled', 'disabled');
      register.removeAttribute('disabled', 'disabled');
      register.setAttribute('value', ' Register ');
      return
    }

    const data = await registerUser.json();
    const {token,...user} = data.data
    const userLoged = await signIn({token: token, accountKey: accountkey.value, user: user});

    if (!userLoged) {
      accountkey.style.border = '1px solid red';
      accountkey.style.borderRadius = '10px';
      accountkey.removeAttribute('disabled', 'disabled');
      register.removeAttribute('disabled', 'disabled');
      register.setAttribute('value', ' Register ');
      return
    }
    setActiveStep(activeStep + 1);
    setSkippedSteps(skippedSteps.filter((skipItem) => skipItem !== activeStep));
  };


  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  return (
    <>
      <Head>
        <title>Cover Your Personal Expenses With Drafts • Qarrington</title>
        <meta
          name="description"
          content="Qarrington is a draft exchange, where you can buy and sell drafts backed by real products. Get started with as little as $12 per month. No fees!"
        />
      </Head>

      <MainContent style={Body}>
        <Grid
          container
          sx={{ height: '100%' }}
          alignItems="stretch"
          spacing={0}
        >
          {/* left container starts */}

          <Grid
            xs={12}
            md={6}
            alignItems="center"
            display="flex"
            justifyContent="center"
            item
          >
            <Container maxWidth="sm">
              <Box style={{ textAlign: 'center' }}>
                <Box sx={{ cursor: 'pointer' }}>
                  <Link href="/">
                    <Image
                      alt="Qarrington Logo"
                      height={32}
                      width={32}
                      src="/assets/media/logos/aa/primary.png"
                    />
                  </Link>
                </Box>

                <Typography
                  fontSize="42px"
                  fontWeight="700"
                  lineHeight="50px"
                  component="div"
                  sx={{ my: 1 }}
                >
                  Cover your expenses with subscriptions
                  <Tooltip
                    title="A draft represents a company's subscription that can be bought, sold, and exchanged by the company's users to access the company's products."
                    placement="top"
                  >
                    <InfoRoundedIcon fontSize="small" color="primary" />
                  </Tooltip>
                </Typography>

                <Typography
                  variant="h6"
                  component="div"
                  color="secondary"
                  padding="0px 20px 0px 20px"
                  gutterBottom
                >
                  Qarrington is a subscription exchange that lets you buy and
                  sell the subscriptions of technology companies. Stocks and
                  cryptos are backed by nothing, subscriptions are backed by
                  real products.
                </Typography>
              </Box>

              <form noValidate autoComplete="on" onSubmit={handleSubmits}>
                <Box
                  style={{ textAlign: 'center', padding: '14px 60px 0px 60px' }}
                >
                  <Stack spacing={1.2} sx={{ width: '100%' }}>
                    {activeStep === steps.length ? (
                      <Stack spacing={1.2} sx={{ width: '100%', mb: 0 }}>
                        <Link href="/subscription/list">
                          <Button
                            size="large"
                            sx={{
                              py: 1.6,
                              textTransform: 'uppercase',
                              fontSize: '13px'
                            }}
                            variant="outlined"
                            fullWidth={true}
                          >
                            i want to fund my business
                          </Button>
                        </Link>

                        <Link href="/">
                          <Button
                            size="large"
                            sx={{
                              color: 'white',
                              py: 1.6,
                              textTransform: 'uppercase',
                              fontSize: '13px'
                            }}
                            variant="contained"
                            fullWidth={true}
                          >
                            i don't have a business
                          </Button>
                        </Link>

                        <Link href="/account/create">
                          <Button
                            style={FormButton}
                            disabled
                            color="secondary"
                            sx={{
                              fontSize: '12px',
                              textTransform: 'uppercase'
                            }}
                          >
                            submitted
                          </Button>
                        </Link>
                      </Stack>
                    ) : (
                      <>
                        {getStepContent(activeStep)}

                        <Button
                          size="large"
                          sx={{
                            color: 'white',
                            py: 1.6,
                            textTransform: 'uppercase',
                            fontSize: '13px'
                          }}
                          variant="contained"
                          fullWidth={true}
                          name="register"
                          type="submit"
                        >
                          {activeStep === steps.length - 1
                            ? 'Register'
                            : 'Next'}
                        </Button>
                        <Button
                          style={FormButton}
                          disabled={activeStep === 0}
                          onClick={handleBack}
                          color="secondary"
                          sx={{ fontSize: '12px', textTransform: 'uppercase' }}
                        >
                          Back
                        </Button>
                      </>
                    )}
                  </Stack>
                </Box>

                <Box textAlign="center">
                  <Typography
                    variant="body2"
                    mt={1}
                    component="div"
                    color="secondary"
                    padding="0px 20px 0px 20px"
                    gutterBottom
                  >
                    By clicking the above button and submitting the above form,
                    I hereby agree to the Service Terms and Privacy Policies
                    governing my use of the Qarrington website.
                  </Typography>
                </Box>
              </form>
            </Container>
          </Grid>

          {/* left container ends */}

          {/* right container starts */}

          <Hidden mdDown>
            <GridWrapper
              xs={12}
              md={6}
              alignItems="center"
              display="flex"
              justifyContent="center"
              item
            >
              <Container maxWidth="sm">
                <Box textAlign="center">
                  <Carousel>
                    {data &&
                      Array.isArray(data) &&
                      data
                        .slice(0, 5)
                        .map(({ _id, accountProfile, accountReview }) => (
                          <Box key={_id}>
                            <Box
                              style={{
                                display: 'flex',
                                justifyContent: 'center'
                              }}
                            >
                              <StyledBadge
                                overlap="circular"
                                anchorOrigin={{
                                  vertical: 'bottom',
                                  horizontal: 'right'
                                }}
                                variant={accountProfile.profileIsActive}
                              >
                                <Avatar
                                  style={{ width: 80, height: 80 }}
                                  alt={accountProfile.profileName}
                                  src={accountProfile.profileAvatar}
                                />
                              </StyledBadge>
                            </Box>
                            <Box marginTop="16px">
                              <Typography
                                variant="h5"
                                component="div"
                                fontWeight="600"
                                gutterBottom
                              >
                                {accountProfile.profileName}
                              </Typography>
                              <Typography
                                variant="body"
                                component="div"
                                gutterBottom
                              >
                                {accountProfile.profileTitle}
                              </Typography>
                              <Typography
                                variant="h4"
                                component="div"
                                fontWeight="600"
                              >
                                {accountReview.reviewContent}
                              </Typography>
                            </Box>
                          </Box>
                        ))}
                  </Carousel>
                </Box>
              </Container>
            </GridWrapper>
          </Hidden>

          {/* right container ends */}
        </Grid>
      </MainContent>
    </>
  );
};

export default Page;

const FormButton = {
  '&:hover': {
    color: 'white',
    backgroundColor: '#f5f5f5'
  }
};

const AccountButton = {
  fontWeight: '800',
  fontSize: '13px',
  padding: '20px 40px 20px 40px',
  textTransform: 'uppercase',
  backgroundColor: 'white',
  color: '#2f3542',
  '&:hover': {
    backgroundColor: '#ffffff90'
  }
};

const MainContent = styled(Box)(
  () => `
    height: 100%;
    display: flex;
    flex: 1;
    overflow: auto;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`
);

const GridWrapper = styled(Grid)(
  ({ theme }) => `
    background: ${theme.colors.gradients.green2};
`
);

const Body = {
  backgroundColor: '#ffffff'
};

const StyledBadge = styled(Badge)(({ theme }) => ({
  '& .MuiBadge-badge': {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: 'ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""'
    }
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0
    }
  }
}));
export async function getServerSideProps(ctx) {
  const { ['token.Qarrington.bearer']: token } = parseCookies(ctx);
 if (token) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      }
    };
 }
  return {
    props: {
    }
  };
}