import dbConnect from '../../../lib/dbConnect';
import Account from '../../../../models/account/Account';
import Jwt from 'jsonwebtoken';

async function handler(req, res) {
  const { method } = req;
  let { id } = req.query;

  dbConnect();
  
  if (method === 'GET') {
    try {
      const idString = new String(id);
      let cid,idt = idString.split('.')
      if(idt.length == 3 || idt.length > 2){
        const { sub: id } = await Jwt.verify(req.query.id, `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`)
        cid = id
      }else{ cid = req.query.id }
      const getUserData = await Account.findById(cid);
      if (getUserData) {
        return res.status(200).json(getUserData);
      }
      return res.status(404).json({ message: 'user not found..' });
    } catch (err) {
      res.status(404).json({ message: 'user not found..'  });
    }
  }

  // update item

  if (method === 'PUT') {
    try {
      const updateItem = await Account.findBy_idAndUpdate(
        _id,
        { $set: req.body },
        { new: true }
      );
      return res.status(200).json(updateItem);
    } catch (err) {
      res.status(400).json(err);
    }
  }

  // delete item

  if (method === 'DELETE') {
    try {
      const deleteItem = await Account.findBy_idAndDelete(_id);
      res.status(200).json(deleteItem);
    } catch (err) {
      res.status(500).json(err);
    }
  }
}

export default handler;

export const config = {
  api: {
    responseLimit: false
  }
};
