import dbConnect from '../../../lib/dbConnect';
import Account from '../../../../models/account/Account';
import Jwt from 'jsonwebtoken';




async function createToken(id) {
  return await Jwt.sign(
    {},
    `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`,
    {
      algorithm: 'HS256',
      expiresIn: '7 days',
      subject: `${id}`
    }
  );
}


async function handler(req, res) {
  const { method } = req;
  

  dbConnect();

  if (method == 'POST') {
    const { accountKey } = JSON.parse(req.body);
    if (!accountKey) return res.status(4).json({ message: 'create' });

    try {
      const findAccount = await Account.findOne({ accountKey: accountKey });

      if (findAccount)
        return res
          .status(401)
          .json({ message: 'error account Key already exists try other..' });

      const user = await Account.create({ accountKey: accountKey });

      /**  gerate token user */
      const data = {
        token: await createToken(user._id),
        ...user._doc,
        isRegister: true
      };
      return res.status(201).json({ data: data, message: 'created' });
    } catch (error) {
      return res
        .status(401)
        .json({ data: '', message: 'something went wrong try again later.' });
    }
  }

  if (method == 'PUT') {
    try {
      const { accountKey } = req.body;
      const { id } = req.body;
      const userAccountKey = await Account.findOne({ accountKey: accountKey });
      if (userAccountKey) {
        return res.status(423).json({ message: 'accountkey already exists.' });
      }
      const user = await Account.findOneAndUpdate( { _id: id },{ accountKey: accountKey },{
        upsert: true,new:true
      });

      const data = {
        token: await createToken(user._id),
        ...user._doc,
        isUpdate: true
      };
      return res.status(201).json(data);

    } catch (error) {
      return res
        .status(500)
        .json({ message: 'Something went wrong, try again later..' });
    }
  }
}

export default handler;

export const config = {
  api: {
    responseLimit: false
  }
};
