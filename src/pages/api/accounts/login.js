import dbConnect from '../../../lib/dbConnect';
import Account from '../../../../models/account/Account';
import Jwt from 'jsonwebtoken';

async function handler(req, res) {
  const { method } = req;
  const { accountKey } = JSON.parse(req.body);
  dbConnect()
  if (method === 'POST') {
    try {
        
      const getAccount = await Account.findOne({ accountKey: accountKey });
      if (!getAccount) {
        return res
          .status(401)
          .json({ message: 'Invalid key , please verify ..' });
      }
      const token = await Jwt.sign({}, `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`, {
        algorithm: 'HS256',
        expiresIn: '7 days',
        subject: `${getAccount._id}`
      });
      const accountData = {
        token: token,
        ...getAccount._doc
      };
      return res.status(200).json(accountData);
    } catch (error) {
      return res.status(401).json({ message: '' , error: error.message });
    }
  }
}

export default handler;
