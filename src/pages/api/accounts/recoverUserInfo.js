import dbConnect from '../../../lib/dbConnect';
import Account from '../../../../models/account/Account';
import Jwt from 'jsonwebtoken';

async function handler(req, res) {
  const { method } = req;
  const { token } = req.body;
  dbConnect();
  if (method == 'POST') {
    try {
      const { sub: id } = Jwt.verify(
        token,
        `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`
      );
      const findUser = await Account.findById(id);
      return res.status(200).json(findUser._doc);
    } catch (error) {
      return res.status(401).json(error);
    }
  }
}

export default handler;
