import dbConnect from '../../../lib/dbConnect';
import Account from '../../../../models/account/Account';
import Jwt from 'jsonwebtoken';
async function handler(req, res) {
  const { method } = req;
  const {
    profileAvatar,
    profileTitle,
    reviewContent,
    step,
    contactAddress,
    contactNumber,
    contactEmail,
    bankRoutingNumber,
    bankAccountNumber,
    bankAccountName,
    bankCurrency,
    bankCountry,
    bankName
  } = req.body;

  
  dbConnect();
  var  data = {} || '';
  try{
  if (method == 'PUT') {
  
    switch (step) {
      case 1:
        data = {
          accountProfile: {
            profileTitle: profileTitle,
            profileAvatar: profileAvatar
          },
          accountReview: {
            reviewContent: reviewContent
          }
        };
        break;
      case 2:
        data = {
          accountContact: {
            contactEmail: contactEmail,
            contactNumber: contactNumber,
            contactAddress: contactAddress
          }
        };
        
        break;
      case 3:
        data = {
            accountBank: {
                bankName: bankName,
                bankCountry: bankCountry,
                bankCurrency: bankCurrency,
                bankAccountName: bankAccountName,
                bankAccountNumber: bankAccountNumber,
                bankRoutingNumber:bankRoutingNumber ,
            },
        }
        break;
    }

    let token = req.headers['authorization'].split(' ')[1];
    const { sub: id } = await Jwt.verify(
      token,
      `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`
    );

    await Account.updateOne({ _id: id }, data);
    return res.status(200).json({ message: 'Success' });
  } 
} catch (err) {
    return res.status(500).Json({message: err.message});
}
}

export default handler;
