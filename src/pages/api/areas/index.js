import Area from '../../../../models/area/Area';
import dbConnect from '../../../lib/dbConnect';

async function handler(req, res) {
  const { method } = req;
  const { name } = req.query;

  dbConnect();

  // read items

  if (method === 'GET') {
    if (name) {
      try {
        const readItems = await Area.findOne({ name: name });
        res.status(200).json(readItems);
      } catch (err) {
        res.status(500).json(err);
      }
    } else {
      try {
        const readItems = await Area.find();
        res.status(200).json(readItems);
      } catch (err) {
        res.status(500).json(err);
      }
    }
  }

  // create item

  if (method === 'POST') {
    try {
      const createItem = await Area.create(req.body);
      res.status(201).json(createItem);
    } catch (err) {
      res.status(500).json(err);
    }
  }
}

export default handler;

export const config = {
  api: {
    responseLimit: false
  }
};
