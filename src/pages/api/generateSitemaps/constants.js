const URL = 'https://www.qarrington.com';
const DATE = new Date().toISOString();
const INDEX_SITEMAP_HEAD = `<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">`;
const INDEX_SITEMAP_TAIL = `</urlset>`;
const MAIN_SITEMAP_HEAD = `<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`;
const MAIN_SITEMAP_TAIL = `</sitemapindex>`;
const LIMITS = {
    area: 50000,
    place: 50000,
    account: 1,
    lang: 1,
    expense: 1,
    draft: 1
}
const STATIC_EXCLUDE = [
    'pages/_app.js',
    'pages/_document.js',
    'pages/404.js',
    'pages/api/'
]
export { URL, DATE, INDEX_SITEMAP_HEAD, INDEX_SITEMAP_TAIL, MAIN_SITEMAP_HEAD, MAIN_SITEMAP_TAIL, STATIC_EXCLUDE, LIMITS };