import Area from '../../../../models/area/Area'
import Place from "../../../../models/place/Place"
import Account from "../../../../models/account/Account"
import Language from "../../../../models/language/Language"
import Expense from "../../../../models/expense/Expense"
import Draft from "../../../../models/draft/Draft"
import dbConnect from '../../../lib/dbConnect'
import { URL, DATE, INDEX_SITEMAP_HEAD, INDEX_SITEMAP_TAIL, STATIC_EXCLUDE, LIMITS } from './constants'

import { writeFileSync } from 'fs'
const prettier = require("prettier");
const formatted = sitemap => prettier.format(sitemap, { parser: "html" });
const glob = require("glob")


async function createIndexedSitemaps() {
  try{


     const pages = glob.sync("src/pages/**/*.js")
     let filteredPages = []

    for(let i = 0; i < pages.length; i++){
      for(let j = 0; j < STATIC_EXCLUDE.length; j++){
        if(pages[i].includes(STATIC_EXCLUDE[j]) || (pages[i].includes('[') && pages[i].includes(']'))) break;
        if(j === STATIC_EXCLUDE.length - 1) filteredPages.push(pages[i])
      }
    }

     const staticArr = [];

     filteredPages.map(page => {
      const path = page.replace("src/pages/", "").replace(".js", "").replace(/\/index/g, "");
      const routePath = path === "index" ? "" : path;
      staticArr.push({
        loc: `${URL}/${routePath}`,
        lastmod: DATE,
        priority: 0.7,
        changefreq: "daily"
      })
    })

    const normalizeXml = string => string?.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;').replaceAll(' ', '-');


    await dbConnect();
  
    const fetchAreas = await Area.find({}).limit(LIMITS.area)
    const fetchPlaces = await Place.find({}).limit(LIMITS.place)
    const fetchAccounts = await Account.find({}).limit(LIMITS.account)
    const fetchLangs = await Language.find({}).limit(LIMITS.lang)
    const fetchExpenses = await Expense.find({}).limit(LIMITS.expense)
    const fetchDrafts = await Draft.find({}).limit(LIMITS.draft)


  const finalAccArr = [];
  const finalLngArr = [];
  const finalPlcArr = [];
  const finalAreaArr = [];
  const finalExpArr = [];
  const finalDrfArr = [];

  let accVar, langVar, expVar, areaVar, draftVar;

  fetchAccounts?.forEach((acc) => {
    accVar = normalizeXml(acc._doc?.info.qid)
    finalAccArr.push({
      loc: `${URL}/u/${accVar}`,
      lastmod: DATE,
      priority: 0.7,
      changefreq: "daily"
    })
  });


  fetchLangs?.forEach((lang) => {
    langVar = normalizeXml(lang.code)
    finalLngArr.push({
      loc: `${URL}/${langVar}`,
      lastmod: DATE,
      priority: 0.7,
      changefreq: "daily"
    })
  });


const placesTo = [];
const placesFrom = [];

fetchPlaces?.forEach(place => {
    placesTo.push(normalizeXml(place.toName))
    placesFrom.push(normalizeXml(place.fromName))
});


placesFrom?.forEach(place1 => {
    placesTo?.forEach(place2 => {
        finalPlcArr.push({
          loc: `${URL}/flight/${place1}-to-${place2}`,
          lastmod: DATE,
          priority: 0.7,
          changefreq: "daily"
        })
        finalPlcArr.push({
          loc: `${URL}/flight/${place1}-from-${place2}`,
          lastmod: DATE,
          priority: 0.7,
          changefreq: "daily"
        })
    })
})


fetchExpenses?.forEach(exp => {
  expVar = normalizeXml(exp.lower)
  finalExpArr.push({
    loc: `${URL}/expenses/${expVar}`,
    lastmod: DATE,
    priority: 0.7,
    changefreq: "daily"
  })
  fetchAreas?.forEach(area => {
    areaVar = normalizeXml(area.name)
    finalExpArr.push({
        loc: `${URL}/expenses/${expVar}/${areaVar}`,
        lastmod: DATE,
        priority: 0.7,
        changefreq: "daily"
      })
  })
})

  fetchDrafts?.forEach((draft) => {
    draftVar = normalizeXml(draft.route)
    finalDrfArr.push({
      loc: `${URL}/dashboard/draft/${draftVar}`,
      lastmod: DATE,
      priority: 0.7,
      changefreq: "daily"
    })
  });

  const allLinks = [...staticArr, ...finalAccArr, ...finalAreaArr, ...finalDrfArr, ...finalExpArr, ...finalLngArr, ...finalPlcArr];

  if(allLinks.length > 25e8) throw new Error("SIZE IS TOO BIG");
  console.log(allLinks.length)
  let indexedSitemapUrls = [];
  let urlCount = 0, sitemapsCount = 0;

  allLinks.forEach(({loc, lastmod, priority, changefreq}, i) => {
    indexedSitemapUrls.push(`<url><loc>${loc}</loc><lastmod>${lastmod}</lastmod><changefreq>${changefreq}</changefreq><priority>${priority}</priority></url>`);
    urlCount++;
    if(urlCount === 5e4 || i == allLinks.length - 1){
        let indexedSitemap = INDEX_SITEMAP_HEAD + indexedSitemapUrls.join('') + INDEX_SITEMAP_TAIL;
        let formattedSitemap = formatted(indexedSitemap);
        writeFileSync(`././././public/sitemap-${sitemapsCount}.xml`, formattedSitemap, "utf8");
        urlCount = 0;
        sitemapsCount++;
        indexedSitemapUrls = [];
    }
    })

    return allLinks.length;
  }
  catch(err){
    return err;
  }
}

export default createIndexedSitemaps;