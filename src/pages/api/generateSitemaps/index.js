import createIndexedSitemaps from './createIndexedSitemaps';
import { URL, MAIN_SITEMAP_HEAD, MAIN_SITEMAP_TAIL } from './constants'

import { writeFileSync } from 'fs'
const prettier = require("prettier");
const formatted = sitemap => prettier.format(sitemap, { parser: "html" });

const robotstxt = require('generate-robotstxt')

async function handler(req, res) {
  try {

    const linksLen = await createIndexedSitemaps();

    if(linksLen instanceof Error) throw linksLen;

    const mainSitemapLen = Math.ceil(linksLen / 5e4);
    const indexedSitemaps = [];


    for(let i = 0; i < mainSitemapLen; i++){
      indexedSitemaps.push(`<sitemap><loc>${URL}/sitemap-${i}.xml</loc></sitemap>`)
    }

    const mainSitemap = MAIN_SITEMAP_HEAD + indexedSitemaps.join('') + MAIN_SITEMAP_TAIL;
    const formattedSitemap = formatted(mainSitemap);

    writeFileSync("././././public/sitemap.xml", formattedSitemap, "utf8");

    

robotstxt({
  policy: [
    {
      userAgent: "*",
      allow: "/",
    }
  ],
  sitemap: `${URL}/sitemap.xml`,
  host: URL,
})
.then(content => {
  writeFileSync("././././public/robots.txt", content, "utf8");
})


    res.status(200).json({ url_size: linksLen, sitemap_size: mainSitemapLen });
  } 
  catch (err) {
    res.status(500).json(err);
  }
}
    
export default handler;

export const config = {
  api: {
    responseLimit: false
  }
};