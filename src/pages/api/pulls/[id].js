import dbConnect from '../../../lib/dbConnect';
import Pull from '../../../../models/pull/Pull';
import Subscription from '../../../../models/subscription/Subscription';
import moment from 'moment';
import Jwt  from 'jsonwebtoken';
import {  destroyCookie ,parseCookies } from 'nookies';

async function handler(req, res) {
  const { method } = req;
  dbConnect();
  // create item
  if (method === 'POST') {
   const { units, subscrition } = req.body;
    try {
      const findSubscrition = await Subscription.findOne({
        subscriptionSlug: subscrition
      });
      if(req.headers['authorization']){
        const token = req.headers['authorization'].split(' ')[1];
      try {
        const { sub: id } = await Jwt.verify(
            token,
          `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`
        );
        if(id == findSubscrition.subscriptionAccountId){
          return res.status(423).json({ message: " You can not pull you subscrition." });
        }
      } catch (error) {
        return res.status(423).json({ message: error.message });
      }
      }
      

      const subscriptionPayout = findSubscrition.subscriptionPrice.currentPrice * units;
      const unitReimating = parseFloat(findSubscrition.subscriptionListing.listingUnits) - units;


      const pullfetch = await Pull.findOne({pullSlug: subscrition})

      if(pullfetch){
       try{
        await Pull.updateOne({pullSlug: subscrition},{
          pullRequests: {
            requestPairId: '',
            requestUnits: pullfetch.pullRequests.requestUnits + units,
            requestPrice: findSubscrition.subscriptionPrice.currentPrice,
            requestAmount: pullfetch.pullRequests.requestAmount + subscriptionPayout,
            requestIsMatched: 'TRUE',
            requestSubmittedAt: moment().toDate(),
            requestMatchedAt: moment().toDate(),
        },
          pullAccount: {
            accountBalance: pullfetch.pullAccount.accountBalance + subscriptionPayout 
          },
          pullUpdatedAt: moment().toDate(),
         }) 

         await Subscription.updateOne({subscriptionSlug: findSubscrition.subscriptionSlug},{
          subscriptionListing: {
            listingUnits: unitReimating
          },
          subscriptionPayout: { payoutAmount: findSubscrition.subscriptionPayout.payoutAmount + subscriptionPayout}
        })
    
        destroyCookie(req, 'token.Qarrington.bearer',{ path:'/'});
        return res.status(200).json({message: 'Subscription updated'});
        } catch(err) {
          return res.status(500).json({ message: err.message});
        }
         
      }


      await Subscription.updateOne({subscriptionSlug: findSubscrition.subscriptionSlug},{
        subscriptionListing: {
          listingUnits: unitReimating
        },
        subscriptionPayout: { payoutAmount: findSubscrition.subscriptionPayout.payoutAmount + subscriptionPayout}
      })
      const Schema = {
        pullSlug: subscrition,
        pullPrice: findSubscrition.subscriptionPrice.currentPrice,
        pullRequests: {
          requestPairId: '',
          requestUnits: units,
          requestPrice: findSubscrition.subscriptionPrice.currentPrice,
          requestAmount: subscriptionPayout,
          requestIsMatched: 'TRUE',
          requestSubmittedAt: moment().toDate(),
          requestMatchedAt: moment().toDate(),
      },
        pullSubscription: {
          subscriptionId: findSubscrition._id,
          subscriptionName: findSubscrition.subscriptionListing.listingName,
          subscriptionLogo: findSubscrition.subscriptionListing.listingLogo,
          subscriptionTicker: findSubscrition.subscriptionListing.listingTicker,
          subscriptionUnits: findSubscrition.subscriptionListing.listingUnits,
          subscriptionBalance: subscriptionPayout
      },
      pullAccount: {
          accountId: findSubscrition.subscriptionAccountId,
          accountBalance: subscriptionPayout ,
      },
        pullUpdatedAt: moment().toDate(),
      }
      
      const createItem = await Pull.create(Schema);
      destroyCookie(req, 'token.Qarrington.bearer',{ path:'/'});
      res.status(201).json(createItem);
    } catch (err) {
      res.status(500).json(err.message);
    }
  }
}

export default handler;

export const config = {
  api: {
    responseLimit: false
  }
};
