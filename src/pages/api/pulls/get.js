import dbConnect from '../../../lib/dbConnect';
import Pull from '../../../../models/push/Push';
import { parseCookies } from 'nookies';
import Jwt from 'jsonwebtoken'
export default async function handler(req, res) {
  const { method } = req;

  dbConnect();

  if (method === 'GET') {
    try{

        const { ['token.Qarrington.bearer']: token } = parseCookies(res);
        let cid;
        if (token) {
            try {
            const { sub: id } = await Jwt.verify(
                token,
                `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`
            );
            cid = id;
            } catch (err) {
            return res.status(423).json({ message: 'Invalid token' });
            }
        }

        const dataschema = {pushAccount: { accountId: cid}} 
        const PullAccountFetch = await Pull.findOne(dataschema )
        console.log( await Pull.find())
        if(PullAccountFetch){
            return res.status(200).json(PullAccountFetch)
        }
        return res.status(404).json({})
    }catch(err){
        return res.status(500).json({message: err.message})
    }
    
  }
}
