import dbConnect from '../../../lib/dbConnect';
import Pull from '../../../../models/pull/Pull';
import Subscription from '../../../../models/subscription/Subscription';
import Jwt  from 'jsonwebtoken';
import Stripe from 'stripe';
import { setCookie } from 'nookies';
import crypto from 'crypto';
import NextCrors from 'nextjs-cors'

async function handler(req, res) {
  const { method } = req;
  const { pullSlug } = req.query;

  dbConnect();

  // read items



  if (method === 'GET') {
    if (pullSlug) {
      try {
        const readItems = await Pull.findOne({ pullSlug: pullSlug });
        res.status(200).json(readItems);
      } catch (err) {
        res.status(500).json(err);
      }
    } else {
      try {
        const readItems = await Pull.find();
        res.status(200).json(readItems);
      } catch (err) {
        res.status(500).json(err);
      }
    }
  }

   

  // create item
  if (method === 'POST') {
    try {
      const { units, subscrition } = req.body;

      const findSubscrition = await Subscription.findOne({
        subscriptionSlug: subscrition
      });

      if(req.headers['authorization']){
        const token = req.headers['authorization'].split(' ')[1];
      try {
        const { sub: id } = await Jwt.verify(
            token,
          `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`
        );
        if(id == findSubscrition.subscriptionAccountId){
          return res.status(423).json({ message: " You can not pull you subscrition." });
        }
      } catch (error) {
        return res.status(423).json({ message: error.message });
      }


      const payment = new Stripe(`${process.env.STRIPE_TEST_SECRET_KEY}`);
      const price = await payment.prices.create({
        currency: 'usd',
        custom_unit_amount: { enabled: true },
        product:findSubscrition.subscriptionListing.listingProductId
      })
       
      const images = []
      images.push(findSubscrition.subscriptionListing.listingLogo)
      const randomid = `${subscrition}-id-${crypto.randomUUID()}`
      const checkout = await payment.checkout.sessions.create({
        payment_method_types:['card'],
        line_items:[
          {
            price_data:{
            currency: 'usd',
            product_data:{
              name: findSubscrition.subscriptionListing.listingTicker,
              images: images,
            },
            unit_amount: parseInt(findSubscrition.subscriptionPrice.launchPrice),
          },
          quantity: units
        },
        ],
        mode: 'payment',
        success_url: `${process.env.NEXT_PUBLIC_APP_URL}/thanks/${randomid}`,
        cancel_url: `${process.env.NEXT_PUBLIC_APP_URL}/subscription/${subscrition}/pull/`
      })
      return res.status(200).json({url:checkout.url,id:randomid});
    }
    }catch(err){
      return res.status(423).json({ message: err.message });
    }
}
}
export default handler;

export const config = {
  api: {
    responseLimit: false
  }
};
