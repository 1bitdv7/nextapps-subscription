import dbConnect from '../../../lib/dbConnect';
import Push from '../../../../models/push/Push';
import { parseCookies } from 'nookies';
import Jwt from 'jsonwebtoken'
export default async function handler(req, res) {
  const { method } = req;

  dbConnect();

  const { ['token.Qarrington.bearer']: token } = parseCookies(res);
  let cid;
  if (token) {
    try {
      const { sub: id } = await Jwt.verify(
        token,
        `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`
      );
      cid = id;
    } catch (err) {
      return res.status(423).json({ message: 'Invalid token' });
    }
  }
  if (method === 'GET') {
    try{
        const dataschema = {pushAccount: { accountId: cid}} 
        const pushAccountFetch = await Push.findOne(dataschema )
        ///console.log( await Push.find())
        if(pushAccountFetch){
            return res.status(200).json(pushAccountFetch)
        }
        return res.status(404).json({})
    }catch(err){
        return res.status(500).json({message: err.message})
    }
    
  }
}
