import dbConnect from '../../../lib/dbConnect';
import Subscription from '../../../../models/subscription/Subscription';
import Stripe from 'stripe';
import Jwt from 'jsonwebtoken'
export default async function handler(req, res) {
  const { method } = req;
  const {
    SubscriptionDetail,
    SubscriptionWebsite,
    SubscriptionLogo,
    SubscriptionPrice,
    SubscriptionSlug,
    SubscriptionTicker,
    SubscriptionName
  } = req.body;
  dbConnect();
  if (method === 'POST') {
    let cid;
    if (req.headers['authorization']) {
      const token = req.headers['authorization'].split(' ')[1];
      try {
        const { sub: id } = await Jwt.verify(
            token,
          `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`
        );
        cid = id;
      } catch (error) {
        return res.status(423).json({ message: error.message });
      }
    }
    try {
      const stripe = new Stripe(`${process.env.STRIPE_TEST_SECRET_KEY}`);
      const account = await stripe.accounts.create({
        country: 'US',
        type: 'custom',
        capabilities: {
          card_payments: { requested: true },
          transfers: { requested: true }
        }
      });

      const image = []
      image.push(SubscriptionLogo)
      const product = await stripe.products.create({
        name: SubscriptionName ,
        description: SubscriptionDetail,
        images: image,
      })
      const dataSchema = {
        subscriptionSlug: SubscriptionSlug,
        subscriptionListing: {
          listingName: SubscriptionName,
          listingTicker: SubscriptionTicker,
          listingWebsite: SubscriptionWebsite,
          listingDetail: SubscriptionDetail,
          listingLogo: SubscriptionLogo,
          listingUnits: '75265,67',
          listingProductId: product.id
        },
        subscriptionAccountId: cid,
        subscriptionIsApproved: 'true',
        subscriptionAccounts: [{
            accountWhitelistTotal: '',
            accountId: '',
            accountIsCustomer: '',
            accountIsCreated: '',
            accountPulledUnits: '',
            accountPulledPrice: '',
            accountPulledAmount: '',
            accountWhitelistedAt: '',
        }],
        subscriptionPrice: {
          launchPrice: SubscriptionPrice,
          currentPrice: SubscriptionPrice
        },
        subscriptionPayout: {
            payoutAmount: '0',
            payoutIsMade: 'true',
        },
        subscriptionTrack: {
            trackCurrentPrice: SubscriptionPrice,
            trackPointInterval: '6.5',
            trackPercentInterval: '8.4',
            trackVariation: 2
        },
        subscriptionStripeId: account.id,
        subscriptionKey: '',
        subscriptionSubmittedAt: '',
      };
      const createSubscription = await Subscription.create(dataSchema);
      return res.status(201).json(createSubscription);
    } catch (err) {
      return res.status(500).json(err);
    }
  }
}
