import dbConnect from '../../../lib/dbConnect';
import Subscription from '../../../../models/subscription/Subscription';
import Jwt from 'jsonwebtoken';
import { parseCookies } from 'nookies';
export default async function handler(req, res) {
  const { method } = req;
    
  dbConnect()

  if (method == 'GET') {
    
    let cid;
    const {['token.Qarrington.bearer']: token} = parseCookies(res)
    if (token) {
      try {
        const { sub: id } = Jwt.verify(
          token,
          `783465iuwyeruiwery384765$$%%&&&**7846583iuweyrwri`
        );
        cid = id;
      } catch (err) {
        return res.status(423).json({ message: 'Invalid token' });
      }
     

      try {
        const getSubscription = await Subscription.findOne({ subscriptionAccountId: cid });     
        return res.status(200).json(getSubscription);
      } catch (err) {
        return res.status(500).json({ message: err.message});
      }
    }
  }
}
