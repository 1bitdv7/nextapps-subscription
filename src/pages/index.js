import React, { Component, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import HeaderMenu from '../components/menus/HeaderMenu';
import LeftGrid from '../components/grids/LeftGrid';
import RightGrid from '../components/grids/RightGrid';
import {
    Avatar,
    Badge,
    Box,
    Breadcrumbs,
    Button,
    ButtonGroup,
    Card,
    CardMedia,
    Container,
    Grid,
    List,
    ListItem,
    Stack,
    styled,
    Tab,
    TextField,
    Tooltip,
    Typography
} from '@mui/material';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import TabContext from '@mui/lab/TabContext';
import Footer from '../components/main/Footer';
import useSWR from 'swr';
import { Pagination } from '@mui/lab';
import Carousel from 'react-material-ui-carousel';

const Page = () => {

    const fetcher = (...args) => fetch(...args).then(res => res.json());
    const { data: accounts } = useSWR(`${process.env.NEXT_PUBLIC_APP_URL}/api/accounts`, fetcher);
    const { data: subscriptions,isLoading } = useSWR(`${process.env.NEXT_PUBLIC_APP_URL}/api/subscriptions`, fetcher);

    if(isLoading) return <p> loading... </p>;

    return (

        <div>

            <Head>
                <title>Qarrington • Qarrington</title>
                <meta
                    name="description"
                    content="Qarrington is a subscription exchange that lets you buy and sell the subscriptions of your favorite technology companies without fees. Register without email!"
                />
            </Head>

            <HeaderMenu />

            <Container>

                <Grid container spacing={2}>

                    {/* LeftGrid Starts Here */}

                    <Grid item xs={12} md={6} lg={3}>
                        <LeftGrid />
                    </Grid>

                    {/* LeftGrid Ends Here */}

                    <Grid item xs={12} md={6} lg={6} mb={4}>
                        <Grid container spacing={1}>

                            <Grid item xs={12}>

                                <Grid container spacing={2}>

                                    {subscriptions && Array.isArray(subscriptions) && subscriptions.map(({ _id, subscriptionSlug, subscriptionListing, subscriptionPrice, subscriptionTrack, subscriptionAccounts, subscriptionPayout, subscriptionKey }) => (
                                        <Grid key={_id} item xs={12} sm={6} md={6} lg={4}>
                                            <Link href={`/subscription/${subscriptionSlug}`}>
                                                <Card style={{ padding: '40px', cursor: 'pointer' }}>
                                                    <Box
                                                        style={{
                                                            display: 'flex',
                                                            justifyContent: 'center'
                                                        }}
                                                    >
                                                        <Avatar
                                                            style={{ width: 40, height: 40 }}
                                                            alt={subscriptionListing.listingName}
                                                            src={subscriptionListing.listingLogo}
                                                        />
                                                    </Box>
                                                    <Box style={{ textAlign: 'center' }}>
                                                        <Tooltip title={subscriptionListing.listingName} placement="top">
                                                            <Box>
                                                                <Box mt={1} mb={0.5}>
                                                                    <Typography component="span" variant="h6" color={subscriptionTrack.trackVariation} fontWeight={700}>
                                                                        {subscriptionTrack.trackPercentInterval}
                                                                    </Typography>
                                                                    <Typography component="span" variant="body2" fontWeight={500} color="secondary">
                                                                        %
                                                                    </Typography>
                                                                </Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary">
                                                                        {subscriptionListing.listingTicker}
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Tooltip>
                                                    </Box>
                                                </Card>
                                            </Link>
                                        </Grid>
                                    ))}

                                </Grid>

                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid item xs={12} md={6} lg={3}>
                        <RightGrid />
                    </Grid>

                </Grid>
                <Footer />
            </Container>

        </div>

    )
}

export default Page

const StyledBadge = styled(Badge)(({ theme }) => ({
    '& .MuiBadge-badge': {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: 'ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""',
        },
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.8)',
            opacity: 1,
        },
        '100%': {
            transform: 'scale(2.4)',
            opacity: 0,
        },
    },
}));

const TabsWrapper = styled(TabList)(
    ({ theme }) => `
          &.MuiTabs-root {
            height: 0;
          }
    `
);

const TabLabel = styled(Tab)(
    ({ theme }) => `
          font-size: 12px;
          font-weight: 700;
          text-transform: uppercase;
    `
);

const DraftBadge = styled(Badge)(({ theme }) => ({
    '& .MuiBadge-badge': {
        right: 0,
        top: -8,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px',
    },
}));