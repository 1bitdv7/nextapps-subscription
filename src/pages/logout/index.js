import {  destroyCookie } from 'nookies';
export default  function Page  () {
  
  return ( <></>);
};

export async function getServerSideProps(ctx) {
  destroyCookie(ctx, 'token.Qarrington.bearer',{ path:'/'});
  return {
    redirect: {
      destination: '/account/access',
      permanent: false,
    }
  };
}
