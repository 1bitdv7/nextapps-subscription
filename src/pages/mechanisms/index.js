import React, { useState } from 'react';
import Head from 'next/head';
import Image from 'next/image';
import HeaderMenu from '../../components/menus/HeaderMenu';
import RightGrid from '../../components/grids/RightGrid';
import { Box, Card, Container, Grid, styled, Tab, Tooltip, Typography } from '@mui/material';
import Footer from '../../components/main/Footer';
import Link from 'next/link';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import TabContext from '@mui/lab/TabContext';

const Page = () => {

    const [mechanism, setMechanism] = useState('2');
    const handleMechanism = (event, newMechanism) => {
        setMechanism(newMechanism);
    }

    return (

        <div>

            <Head>
                <title>
                    Mechanisms • Qarrington
                </title>
                <meta
                    name="description"
                    content="Qarrington is a subscription exchange that lets you buy and sell the subscriptions of your favorite technology companies without fees. Register without email!"
                />
            </Head>

            <HeaderMenu />

            <Container>
                <Grid container spacing={2}>

                    <Grid item xs={12} md={6} lg={9} mb={4}>
                        <Grid container spacing={1}>

                            <Grid item xs={12}>

                                {/* intro starts */}

                                <Grid item xs={12} mb={2}>
                                    <Grid container spacing={2}>

                                        <Grid item xs={12} sm={6} md={6} lg={6}>
                                            <Link href="/account/create">
                                                <Card style={{ padding: '60px', color: 'white', backgroundColor: '#2f3542', cursor: 'pointer' }}>
                                                    <Typography variant="body" fontWeight={600}>
                                                        Qarrington is an easy-to-use exchange. See how it works for businesses and consumers.
                                                    </Typography>
                                                </Card>
                                            </Link>
                                        </Grid>

                                        <Grid item xs={12} sm={6} md={6} lg={6}>
                                            <Link href="/account/create">
                                                <Card style={{ padding: '60px', color: 'white', backgroundColor: '#2ed573', cursor: 'pointer' }}>
                                                    <Typography variant="body" fontWeight={600}>
                                                        If you're new to Qarrington and you don't have an account, you can get started in 2 seconds.
                                                    </Typography>
                                                </Card>
                                            </Link>
                                        </Grid>

                                    </Grid>
                                </Grid>

                                {/* intro ends */}

                                {/* mechanism tab starts */}

                                <TabContext value={mechanism}>

                                    <Box sx={{ width: '100%', mb: 2, display: 'flex', justifyContent: 'center' }}>
                                        <TabsWrapper
                                            onChange={handleMechanism}
                                            indicatorColor="transparent"
                                            TabIndicatorProps={{
                                                sx: { backgroundColor: 'transparent', height: 4 }
                                            }}
                                            sx={{
                                                "& button:hover": { backgroundColor: "#c7c7c7" },
                                                "& button:active": { backgroundColor: "#b6b6b6" },
                                                "& button.Mui-selected": { backgroundColor: "#a7a7a7" },
                                                "& div.MuiTabs-scroller": { overflowY: "auto" },
                                            }}
                                            scrollButtons="auto"
                                            aria-label="scrollable auto tabs example"
                                        >
                                            <TabLabel label="Business" value="1" />
                                            <TabLabel label="Consumer" value="2" />
                                            <TabLabel label="Platform" value="3" />
                                        </TabsWrapper>
                                    </Box>

                                    <Box style={{ marginBottom: '0px' }}>

                                        {/* business tab starts */}

                                        <TabPanel sx={{ padding: 0 }} value="1">

                                            <Grid item xs={12} mb={2}>
                                                <Grid container spacing={2}>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '0px' }}>
                                                            <Box
                                                                style={{
                                                                    display: 'flex',
                                                                    justifyContent: 'center'
                                                                }}
                                                            >
                                                                <Image
                                                                    style={{ width: '100%', height: '100%' }}
                                                                    alt="Create, launch, and list your first subscription."
                                                                    src="/assets/media/mechanisms/launch.png"
                                                                    height={282}
                                                                    width={450}
                                                                    objectFit="cover"
                                                                />
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '60px' }}>
                                                            <Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary" textTransform="uppercase">
                                                                        launch
                                                                    </Typography>
                                                                    <Link href="/account/create">
                                                                        <Typography variant="h4" fontWeight={700} my={1.2} sx={CardTitle}>
                                                                            Create, launch, and list your first subscription.
                                                                        </Typography>
                                                                    </Link>
                                                                    <Typography variant="body" fontWeight={500} color="secondary">
                                                                        First, you must create a subscription and connect your Stripe or bank account to facilitate the payouts after the launch.
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '60px' }}>
                                                            <Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary" textTransform="uppercase">
                                                                        advance
                                                                    </Typography>
                                                                    <Link href="/account/create">
                                                                        <Typography variant="h4" fontWeight={700} my={1.2} sx={CardTitle}>
                                                                            Whitelist customers for your subscription launch.
                                                                        </Typography>
                                                                    </Link>
                                                                    <Typography variant="body" fontWeight={500} color="secondary">
                                                                        After that, the subscription will run for a maximum of 7 days. During this period, your customers can buy at least 12 units at a time.
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '0px' }}>
                                                            <Box
                                                                style={{
                                                                    display: 'flex',
                                                                    justifyContent: 'center'
                                                                }}
                                                            >
                                                                <Image
                                                                    style={{ width: '100%', height: '100%' }}
                                                                    alt="Whitelist customers for your subscription launch."
                                                                    src="/assets/media/mechanisms/advance.png"
                                                                    height={282}
                                                                    width={450}
                                                                    objectFit="cover"
                                                                />
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '0px' }}>
                                                            <Box
                                                                style={{
                                                                    display: 'flex',
                                                                    justifyContent: 'center'
                                                                }}
                                                            >
                                                                <Image
                                                                    style={{ width: '100%', height: '100%' }}
                                                                    alt="Onboard your customers with powerful REST APIs."
                                                                    src="/assets/media/mechanisms/create.png"
                                                                    height={282}
                                                                    width={450}
                                                                    objectFit="cover"
                                                                />
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '60px' }}>
                                                            <Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary" textTransform="uppercase">
                                                                        create
                                                                    </Typography>
                                                                    <Link href="/account/create">
                                                                        <Typography variant="h4" fontWeight={700} my={1.2} sx={CardTitle}>
                                                                            Onboard your customers with powerful REST APIs.
                                                                        </Typography>
                                                                    </Link>
                                                                    <Typography variant="body" fontWeight={500} color="secondary">
                                                                        Once you receive payouts, you'd need the Create API to create customers and grant the customers access to your products.
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                </Grid>
                                            </Grid>

                                        </TabPanel>

                                        {/* business tab ends */}

                                        {/* consumer tab starts */}

                                        <TabPanel sx={{ padding: 0 }} value="2">

                                            <Grid item xs={12} mb={2}>
                                                <Grid container spacing={2}>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '0px' }}>
                                                            <Box
                                                                style={{
                                                                    display: 'flex',
                                                                    justifyContent: 'center'
                                                                }}
                                                            >
                                                                <Image
                                                                    style={{ width: '100%', height: '100%' }}
                                                                    alt="Buy subscriptions during and after a new listing."
                                                                    src="/assets/media/mechanisms/pull.png"
                                                                    height={282}
                                                                    width={450}
                                                                    objectFit="cover"
                                                                />
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '60px' }}>
                                                            <Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary" textTransform="uppercase">
                                                                        pull
                                                                    </Typography>
                                                                    <Link href="/account/create">
                                                                        <Typography variant="h4" fontWeight={700} my={1.2} sx={CardTitle}>
                                                                            Buy subscriptions during and after a new listing.
                                                                        </Typography>
                                                                    </Link>
                                                                    <Typography variant="body" fontWeight={500} color="secondary">
                                                                        Primarily, you can buy subscriptions during the launch of a new listing or from existing customers after the subscription listing.
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '60px' }}>
                                                            <Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary" textTransform="uppercase">
                                                                        access
                                                                    </Typography>
                                                                    <Link href="/account/create">
                                                                        <Typography variant="h4" fontWeight={700} my={1.2} sx={CardTitle}>
                                                                            Use your subscription to access online services.
                                                                        </Typography>
                                                                    </Link>
                                                                    <Typography variant="body" fontWeight={500} color="secondary">
                                                                        Next, you can access the company's products with your Qarrington Identity; Qid. Each subscription unit is a month of access.
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '0px' }}>
                                                            <Box
                                                                style={{
                                                                    display: 'flex',
                                                                    justifyContent: 'center'
                                                                }}
                                                            >
                                                                <Image
                                                                    style={{ width: '100%', height: '100%' }}
                                                                    alt="Use your subscription to access online services."
                                                                    src="/assets/media/mechanisms/access.png"
                                                                    height={282}
                                                                    width={450}
                                                                    objectFit="cover"
                                                                />
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '0px' }}>
                                                            <Box
                                                                style={{
                                                                    display: 'flex',
                                                                    justifyContent: 'center'
                                                                }}
                                                            >
                                                                <Image
                                                                    style={{ width: '100%', height: '100%' }}
                                                                    alt="Sell your subscription and get paid via bank transfer."
                                                                    src="/assets/media/mechanisms/push.png"
                                                                    height={282}
                                                                    width={450}
                                                                    objectFit="cover"
                                                                />
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '60px' }}>
                                                            <Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary" textTransform="uppercase">
                                                                        push
                                                                    </Typography>
                                                                    <Link href="/account/create">
                                                                        <Typography variant="h4" fontWeight={700} my={1.2} sx={CardTitle}>
                                                                            Sell your subscription and get paid via bank transfer.
                                                                        </Typography>
                                                                    </Link>
                                                                    <Typography variant="body" fontWeight={500} color="secondary">
                                                                        Once you sell your subscription, you'd receive payouts via bank transfer. But you might lose your access to the company's products.
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                </Grid>
                                            </Grid>

                                        </TabPanel>

                                        {/* consumer tab ends */}

                                        {/* platform tab starts */}

                                        <TabPanel sx={{ padding: 0 }} value="3">

                                            <Grid item xs={12} mb={2}>
                                                <Grid container spacing={2}>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '0px' }}>
                                                            <Box
                                                                style={{
                                                                    display: 'flex',
                                                                    justifyContent: 'center'
                                                                }}
                                                            >
                                                                <Image
                                                                    style={{ width: '100%', height: '100%' }}
                                                                    alt="Get the most accurate subscription data."
                                                                    src="/assets/media/mechanisms/fetch.png"
                                                                    height={282}
                                                                    width={450}
                                                                    objectFit="cover"
                                                                />
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '60px' }}>
                                                            <Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary" textTransform="uppercase">
                                                                        fetch
                                                                    </Typography>
                                                                    <Link href="/account/create">
                                                                        <Typography variant="h4" fontWeight={700} my={1.2} sx={CardTitle}>
                                                                            Get the most accurate subscription data.
                                                                        </Typography>
                                                                    </Link>
                                                                    <Typography variant="body" fontWeight={500} color="secondary">
                                                                        Fetch reliable subscription data from the world's first subscription exchange. Get started today with a unique Private Key.
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '60px' }}>
                                                            <Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary" textTransform="uppercase">
                                                                        integrate
                                                                    </Typography>
                                                                    <Link href="/account/create">
                                                                        <Typography variant="h4" fontWeight={700} my={1.2} sx={CardTitle}>
                                                                            Link accurate subscription data to your products.
                                                                        </Typography>
                                                                    </Link>
                                                                    <Typography variant="body" fontWeight={500} color="secondary">
                                                                        Integrate timely subscription data into your products to quickly track the world's most powerful and accurate subscription data.
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '0px' }}>
                                                            <Box
                                                                style={{
                                                                    display: 'flex',
                                                                    justifyContent: 'center'
                                                                }}
                                                            >
                                                                <Image
                                                                    style={{ width: '100%', height: '100%' }}
                                                                    alt="Link accurate subscription data to your products."
                                                                    src="/assets/media/mechanisms/integrate.png"
                                                                    height={282}
                                                                    width={450}
                                                                    objectFit="cover"
                                                                />
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '0px' }}>
                                                            <Box
                                                                style={{
                                                                    display: 'flex',
                                                                    justifyContent: 'center'
                                                                }}
                                                            >
                                                                <Image
                                                                    style={{ width: '100%', height: '100%' }}
                                                                    alt="Launch subscription products thru REST API."
                                                                    src="/assets/media/mechanisms/track.png"
                                                                    height={282}
                                                                    width={450}
                                                                    objectFit="cover"
                                                                />
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6} md={6} lg={6}>
                                                        <Card style={{ padding: '60px' }}>
                                                            <Box>
                                                                <Box>
                                                                    <Typography variant="body2" fontWeight={700} color="secondary" textTransform="uppercase">
                                                                        track
                                                                    </Typography>
                                                                    <Link href="/account/create">
                                                                        <Typography variant="h4" fontWeight={700} my={1.2} sx={CardTitle}>
                                                                            Launch subscription products thru REST API.
                                                                        </Typography>
                                                                    </Link>
                                                                    <Typography variant="body" fontWeight={500} color="secondary">
                                                                        Track subscription data from the world's largest subscription exchange and ship better subscription products through REST API.
                                                                    </Typography>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Grid>

                                                </Grid>
                                            </Grid>

                                        </TabPanel>

                                        {/* platform tab ends */}

                                    </Box>

                                </TabContext>

                                {/* tabs ends */}

                                <Footer />
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid item xs={12} md={6} lg={3}>
                        <RightGrid />
                    </Grid>

                </Grid>
            </Container>

        </div>

    )
}

export default Page

const TabsWrapper = styled(TabList)(
    ({ theme }) => `
          &.MuiTabs-root {
            height: 0;
          }
    `
);

const TabLabel = styled(Tab)(
    ({ theme }) => `
          font-size: 12px;
          font-weight: 700;
          text-transform: uppercase;
    `
);

const CardTitle = {
    cursor: 'pointer',
    color: '#2ed573',
    '&:hover': {
        color: '#000000'
    }
};
