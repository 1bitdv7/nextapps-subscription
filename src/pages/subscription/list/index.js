import React, { useState } from "react";
import Router from "next/router";
import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';
import Carousel from 'react-material-ui-carousel';
import InfoRoundedIcon from '@mui/icons-material/InfoRounded';
import { Avatar, Badge, Box, Button, Container, Grid, Hidden, Stack, styled, TextField, Tooltip, Typography } from '@mui/material';
import useSWR from 'swr';
import { getServerSideProps } from '../../../lib/getServerSideProps';
import Api from "../../../lib/fetchApi";




const Page = () => {

  const fetcher = (...args) => fetch(...args).then(res => res.json());
  const { data } = useSWR(`${process.env.NEXT_PUBLIC_APP_URL}/api/accounts`, fetcher)

  const [activeStep, setActiveStep] = useState(0);
  const [skippedSteps, setSkippedSteps] = useState([]);
  const [formData, setformData] = useState([])

  const getSteps = () => {
    return [
      "Subscription Name",
      "Subscription Ticker",
      "Subscription Slug",
      "Subscription Price",
      "Subscription Logo",
      "Subscription Website",
      "Subscription Detail",
    ];
  }

 

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <Stack spacing={1.2} sx={{ width: '100%' }}>
            <Tooltip title="Kindly provide your subscription name, which should be your company's name." placement="top">
              <TextField
                sx={{ input: { textAlign: "center" } }}
                required
                defaultValue=""
                placeholder="Subscription Name"
                name="SubscriptionName"
              />
            </Tooltip>
          </Stack>
        );

      case 1:
        return (
          <Stack spacing={1.2} sx={{ width: '100%' }}>
            <Tooltip title="Kindly enter unique two-to-four letters as your subscription ticker or symbol." placement="top">
              <TextField
                sx={{ input: { textAlign: "center" } }}
                required
                defaultValue="-"
                name="SubscriptionTicker"
                placeholder="Subscription Ticker"
              />
            </Tooltip>
          </Stack>
        );

      case 2:
        return (
          <Stack spacing={1.2} sx={{ width: '100%' }}>
            <Tooltip title="Kindly provide your subscription slug in small caps without spaces or symbols." placement="top">
              <TextField
                sx={{ input: { textAlign: "center" } }}
                required
                name="SubscriptionSlug"
                placeholder="Subscription Slug"
              />
            </Tooltip>
          </Stack>
        );

      case 3:
        return (
          <Stack spacing={1.2} sx={{ width: '100%' }}>
            <Tooltip title="Kindly enter your subscription price; the average monthly cost of your products." placement="top">
              <TextField
                sx={{ input: { textAlign: "center" } }}
                required
                name="SubscriptionPrice"
                placeholder="Subscription Price"
              />
            </Tooltip>
          </Stack>
        );

      case 4:
        return (
          <Stack spacing={1.2} sx={{ width: '100%' }}>
            <Tooltip title="Kindly enter the URL of your subscription logo, which is also your company's logo." placement="top">
              <TextField
                sx={{ input: { textAlign: "center" } }}
                required
                name="SubscriptionLogo"
                placeholder="Subscription Logo"
              />
            </Tooltip>
          </Stack>
        );

      case 5:
        return (
          <Stack spacing={1.2} sx={{ width: '100%' }}>
            <Tooltip title="Kindly enter your subscription website, which is also your company's website." placement="top">
              <TextField
                sx={{ input: { textAlign: "center" } }}
                required
                name="SubscriptionWebsite"
                placeholder="Subscription Website"
              />
            </Tooltip>
          </Stack>
        );

      case 6:
        return (
          <Stack spacing={1.2} sx={{ width: '100%' }}>
            <Tooltip title="Kindly provide your subscription detail or a description of your business model." placement="top">
              <TextField
                sx={{ input: { textAlign: "center" } }}
                required
                name="SubscriptionDetail"
                placeholder="Subscription Detail"
              />
            </Tooltip>
          </Stack>
        );

      default:
        return "unknown step";
    }
  }


  const [subscriptions,SetSubscription] = useState([])
  const handleNext = () => {
    setActiveStep(activeStep + 1);
    setSkippedSteps(skippedSteps.filter((skipItem) => skipItem !== activeStep));
  };

  const steps = getSteps();
  const handlerSubmit = async (event)=>{
    event.preventDefault();
     let dataFromTarget =  {};
     dataFromTarget = {[event.target[0].name]:`${event.target[0].value}`}
     SetSubscription(subscriptions=>({...subscriptions,...dataFromTarget}))
    handleNext()
  }
  const handlerClickConect = async (e)=>{
    e.preventDefault();
    const accounAdd = await Api('/api/subscriptions/create','POST',subscriptions)

    if( accounAdd.code != 201){
       // error 
       return
    }
    // redirect
   await Router.push('/subscription')

  }

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  return (

    <>

      <Head>
        <title>Fund Your Business Operations With Subscriptions • Qarrington</title>
        <meta
          name="description"
          content="Qarrington is a subscription exchange, where technology companies advance subscriptions to customers and fund business operations without equity dilution."
        />
      </Head>

      <MainContent style={Body}>

        <Grid
          container
          sx={{ height: '100%' }}
          alignItems="stretch"
          spacing={0}
        >

          {/* left container starts */}

          <Grid
            xs={12}
            md={6}
            alignItems="center"
            display="flex"
            justifyContent="center"
            item
          >
            <Container maxWidth="sm">

              <Box style={{ textAlign: 'center' }}>

                <Box sx={{ cursor: 'pointer' }}>
                  <Link href="/">
                    <Image
                      alt="Qarrington Logo"
                      height={32}
                      width={32}
                      src="/assets/media/logos/aa/primary.png"
                    />
                  </Link>
                </Box>

                <Typography fontSize="42px" fontWeight="700" lineHeight="50px" component="div" sx={{ my: 1 }}>
                  Finance your business with subscriptions
                  <Tooltip title="Subscriptions only give your customers access to use your products & services, they do not represent investment stakes." placement="top">
                    <InfoRoundedIcon fontSize="small" color="primary" />
                  </Tooltip>
                </Typography>

                <Typography variant="h6" component="div" color="secondary" padding="0px 20px 0px 20px" gutterBottom>
                  Qarrington is a subscription exchange that allows technology companies to raise funds by advancing their subscriptions to customers. Think stocks & cryptos, but backed with your products.
                </Typography>

              </Box>


                <Box style={{ textAlign: 'center', padding: '14px 60px 0px 60px' }}>

                  <Stack spacing={1.2} sx={{ width: '100%' }}>

                    {activeStep === steps.length ? (
                    <form noValidate autoComplete="on">
                      <Stack spacing={1.2} sx={{ width: '100%', mb: 0 }}>

                        <Link href="/help">
                          <Button
                            size="large"
                            sx={{ py: 1.6, textTransform: 'uppercase', fontSize: '13px' }}
                            variant="outlined"
                            fullWidth={true}
                          >
                            get in touch
                          </Button>
                        </Link>

                        
                          <Button
                            size="large"
                            sx={{ color: 'white', py: 1.6, textTransform: 'uppercase', fontSize: '13px' }}
                            variant="contained"
                            fullWidth={true}
                            onClick={handlerClickConect}
                          >
                            Gerete Stripe Account
                          </Button>
                       

                        <Link href="/account/create">
                          <Button
                            style={FormButton}
                            disabled
                            color="secondary"
                            sx={{ fontSize: '12px', textTransform: 'uppercase' }}
                          >
                            submitted
                          </Button>
                        </Link>

                      </Stack>
                      </form>

                    ) : (
                      <>
                        <form  onSubmit={handlerSubmit}>
                        {getStepContent(activeStep)}
                          { activeStep === steps.length - 1 ? <>
                            <Button
                          size="large"
                          sx={{ color: 'white', py: 1.6, textTransform: 'uppercase', fontSize: '13px' }}
                          variant="contained"
                          fullWidth={true}
                          type="submit"
                        >
                          Done
                        </Button>
                          </> : <>
                          <Button
                          size="large"
                          sx={{ color: 'white', py: 1.6, textTransform: 'uppercase', fontSize: '13px' }}
                          variant="contained"
                          fullWidth={true}
                          type="submit"
                          
                        >
                         NEXT
                        </Button>
                          </>}
                          
                          
                        <Button
                          style={FormButton}
                          disabled={activeStep === 0}
                          onClick={handleBack}
                          color="secondary"
                          sx={{ fontSize: '12px', textTransform: 'uppercase' }}
                        >
                          Back
                        </Button>
                        </form>
                      </>
                    )}

                  </Stack>

                </Box>

                <Box textAlign="center">
                  <Typography variant="body2" mt={1} component="div" color="secondary" padding="0px 20px 0px 20px" gutterBottom>
                    By clicking the above button and submitting the above form, I hereby agree to the Service Terms and Privacy Policies governing my use of the Qarrington website.
                  </Typography>
                </Box>

              
            </Container>
          </Grid>

          {/* left container ends */}

          {/* right container starts */}

          <Hidden mdDown>
            <GridWrapper
              xs={12}
              md={6}
              alignItems="center"
              display="flex"
              justifyContent="center"
              item
            >

              <Container maxWidth="sm">
                <Box textAlign="center">
                  <Carousel>
                    {data && Array.isArray(data) && data.slice(0, 5).map(({ _id, accountProfile, accountReview }) => (
                      <Box key={_id}>
                        <Box
                          style={{
                            display: 'flex',
                            justifyContent: 'center'
                          }}
                        >
                          <StyledBadge
                            overlap="circular"
                            anchorOrigin={{
                              vertical: 'bottom',
                              horizontal: 'right'
                            }}
                            variant={accountProfile.profileIsActive}
                          >
                            <Avatar
                              style={{ width: 80, height: 80 }}
                              alt={accountProfile.profileName}
                              src={accountProfile.profileAvatar}
                            />
                          </StyledBadge>
                        </Box>
                        <Box marginTop="16px">
                          <Typography variant="h5" component="div" fontWeight="600" gutterBottom>{accountProfile.profileName}</Typography>
                          <Typography variant="body" component="div" gutterBottom>{accountProfile.profileTitle}</Typography>
                          <Typography variant="h4" component="div" fontWeight="600">{accountReview.reviewContent}</Typography>
                        </Box>
                      </Box>
                    ))}
                  </Carousel>
                </Box>
              </Container>

            </GridWrapper>
          </Hidden>

          {/* right container ends */}

        </Grid>
      </MainContent>

    </>

  );

}

export default Page;

const FormButton = {
  "&:hover": {
    color: 'white',
    backgroundColor: '#f5f5f5'
  },
};

const AccountButton = {
  fontWeight: '800',
  fontSize: '13px',
  padding: '20px 40px 20px 40px',
  textTransform: 'uppercase',
  backgroundColor: 'white',
  color: '#2f3542',
  '&:hover': {
    backgroundColor: '#ffffff90'
  }
};

const MainContent = styled(Box)(
  () => `
    height: 100%;
    display: flex;
    flex: 1;
    overflow: auto;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`
);

const GridWrapper = styled(Grid)(
  ({ theme }) => `
    background: ${theme.colors.gradients.green2};
`
);

const Body = {
  backgroundColor: "#ffffff"
};

const StyledBadge = styled(Badge)(({ theme }) => ({
  '& .MuiBadge-badge': {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: 'ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}));


export {getServerSideProps}