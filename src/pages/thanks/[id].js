import React from 'react';
import { parseCookies } from 'nookies';
import Api from '../../lib/fetchApi';

const Page = () => {

  const { [id]: token, ['token.Qarrington.bearer']:Bearer } = parseCookies();
  
  return <>processing wait... {token}, {Bearer}</>;
};

export default Page;

export async function getServerSideProps(req,res) {
  const { id } = req.params;
  const { [id]: token, ['token.Qarrington.bearer']:Bearer } = parseCookies(req);
    //const {units,subscrition} = JSON.parse(token)
   
    if (!token) {
        return {
        
        }
    }
    const results = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/api/pulls/${id}`,{
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + Bearer },
        body: token
    })
    if(results.status == 200 || results.status == 201){
        return {
            redirect: {
                destination: '/dashboard',
                permanent: false,
              }
            }
    }
  return {
      props: {
      }
    };
}
